import java.util.Scanner;

public class Salary {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int [] days = new int[7];
        for (int i = 0; i < 7; i++)
        {
            days[i] = sc.nextInt();
        }

        double totalSalary = 0;
        int totalHours = 0;
        for (int i = 0; i < 7; i++)
        {
            totalSalary += (days[i] * 100);
            if (days[i] > 8) {
                totalSalary += (days[i] - 8) * 15;
            }
            totalHours += days[i];
        }
        if(totalHours >40)
        {
            totalSalary += (totalHours -40)*25;
        }
        else
        {
            totalSalary += (days[0]*100)*0.50;
            totalSalary += (days[6]*100)*0.25;
        }

        System.out.println(totalSalary);

    }
}

